=============================================
  Symja Library - Java Symbolic Math System
=============================================

Features:
* arbitrary precision integers, rational and complex numbers
* differentiation, integration, polynomial and linear algebra functions...
* a general purpose Term Rewriting System and Pattern Matching engine
* use human readable math expression strings or the internal abstract syntax tree (AST) representation to code in Java.
* see the "Getting started with Symja" document on the Wiki pages
   
Online demo: 
* http://symjaweb.appspot.com/ (Mobile web interface)
* http://symjaweb.appspot.com/new.jsp (Notebook interface)

See the Wiki pages:
* https://bitbucket.org/axelclk/symja_android_library/wiki
	
axelclk_AT_gmail_DOT_com 